package com.dmitry.events;

public class Event {
	
	private int id;
	private String title; 
	private String description;
	private long registrationDate;
	private long startDate;
	private long endDate;
	private String geoDescription;
	private Organization organization;
	private Category category;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(long registrationDate) {
		this.registrationDate = registrationDate;
	}
	public long getStartDate() {
		return startDate;
	}
	public void setStartDate(long startDate) {
		this.startDate = startDate;
	}
	public long getEndDate() {
		return endDate;
	}
	public void setEndDate(long endDate) {
		this.endDate = endDate;
	}
	public String getGeoDescription() {
		return geoDescription;
	}
	public void setGeoDescription(String geoDescription) {
		this.geoDescription = geoDescription;
	}
	public Organization getOrganization() {
		return organization;
	}
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}

}
