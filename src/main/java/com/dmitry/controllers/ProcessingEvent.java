package com.dmitry.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import com.dmitry.events.*;



public class ProcessingEvent extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private final static String PATH="com/dmitry/controllers/application.properties";
	private static Properties config;

	private Gson gson = new Gson();
	private ArrayList<Event> events;
	
	
	public void init() throws ServletException {
		
		loadProperties();
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
        URL data = new URL(config.getProperty("url"));
		
		HttpURLConnection connection=null;
		connection =(HttpURLConnection)data.openConnection();
		connection.setRequestMethod("GET");
	    connection.setDoOutput(true);
	    connection.setDoInput(true);
	    connection.connect();
	    
	    Type type = new TypeToken<ArrayList<Event>>(){}.getType();
	    events = gson.fromJson(read(connection.getInputStream()), type);
		
		RequestDispatcher answer=getServletContext().getRequestDispatcher("/index.html");
		PrintWriter out = response.getWriter();
		
		out.println("<!DOCTYPE html>");
		out.println("<html");
		out.println("<head>");
		out.println("<meta charset='UTF-8'>");
		out.println("<title> site</title>");
		out.println("</head>");
		out.println("<body>");
		out.println("<table border='1' bgcolor='green'>");
		out.println("<tr bgcolor='red'>");
		out.println("<td>Id</td>");
		out.println("<td>Title</td>");
		out.println("<td>Description</td>");
		out.println("<td>RegidtrationDate</td>");
		out.println("<td>StartDate</td>");
		out.println("<td>EndDate</td>");
		out.println("<td>GoeDescription</td>");
		out.println("<td>Organization</td>");
		out.println("<td>Category</td></tr>");
	    for(int i=0;i<events.size();i++){
	    	out.println("<tr>");
	    	out.println("<td>"+events.get(i).getId()+"</td>");
	    	out.println("<td>"+events.get(i).getTitle()+"</td>");
	    	out.println("<td>"+events.get(i).getDescription()+"</td>");
	    	out.println("<td>"+parseDate(events.get(i).getRegistrationDate())+"</td>");
	    	out.println("<td>"+parseDate(events.get(i).getStartDate())+"</td>");
	    	out.println("<td>"+parseDate(events.get(i).getEndDate())+"</td>");
	    	out.println("<td>"+events.get(i).getGeoDescription()+"</td>");
	    	out.println("<td>"+events.get(i).getOrganization().getName()+"</td>");
	    	out.println("<td>"+events.get(i).getCategory().getName()+"</td>");
	    	out.println("</tr>");
	    }
	    out.println("</table>");
		out.println("</body>");
		out.println("</html>");
		answer.include(request, response);
	}
	private String read(InputStream in){
		
		String result="";
		int data;
		
		try {
			while((data=in.read())!=-1){
				result=result+(char)data;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return result;
	}
	private String parseDate(long time) {
		
	    return LocalDateTime.ofInstant(Instant.ofEpochMilli(time), ZoneId.systemDefault()).toString();
		
	}
private void loadProperties(){
		
		ClassLoader loader =Thread.currentThread().getContextClassLoader();
		InputStream in =loader.getResourceAsStream(PATH);
		
		try {	
		    config = new Properties();
			config.load(in);
		
		}
		 catch (IOException e) {
			e.printStackTrace();
		}
		finally{
			try {
				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
   

}
